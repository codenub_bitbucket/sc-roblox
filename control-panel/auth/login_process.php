<?php
session_start();
require_once '../koneksi.php';
require_once '../utils/all.php';

if ($_SERVER['REQUEST_METHOD'] !== 'POST') die();

// $RECAPTCHA_TOKEN = $_POST['recaptcha'] ?? die();

// $ch = curl_init();
// curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify?secret=6Lf5lfkbAAAAAHlWVIAg9KvfJlQuzFV5uBasYDfz&response=" . $RECAPTCHA_TOKEN . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// $verifyCaptcha = json_decode(curl_exec($ch));
// curl_close($ch);

// if ($verifyCaptcha->success == FALSE) return send_response(400, "Please complete the captcha.");

$TOKEN = strtolower(filter_var($_POST['token'], FILTER_SANITIZE_STRING));

if ($sql = mysqli_query($koneksi, "SELECT * FROM member WHERE token='$TOKEN'")) {
    $result_rows = $sql->num_rows;
    if ($result_rows > 0) {

        $result = $sql->fetch_assoc();
        $webhook_code = $result['webhook_code'];
        $expired_at = $result['expired_at'];

        $date = new DateTime($expired_at);
        $now = new DateTime();

        if ($date < $now) return send_response(400, "Your subscription has expired.");

        $_SESSION["user_token"] = $TOKEN;

        $sql_profile = $koneksi->query("SELECT * FROM profile WHERE webhook_code='$webhook_code'");
        if ($sql_profile->num_rows === 0) {

            $random_id = mt_rand(10000000, 999999999);
            $random_followers = mt_rand(1, 100) . "k+";

            $array_text = array("Hi!", "Hello!", "Hola!", "Welcome To My Profile!", "Hiii");
            $random_text = array_rand($array_text, 1);
            $encoded_text = base64_encode($array_text[$random_text]);
            
            $sql_insert = $koneksi->query("INSERT INTO profile (webhook_code, fake_id, real_username, fake_username, about, youtube_url, premium, followers) VALUES ('$webhook_code', $random_id, 'Roblox', '$webhook_code', '$encoded_text', 'https://www.youtube.com', '1', '$random_followers')");
        }

        return send_response(200, "Login successfully, you will be redirected automatically.");
    } else {
        return send_response(400, "Invalid token: $TOKEN");
    }
};
