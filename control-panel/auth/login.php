<?php
session_start();
require_once '../koneksi.php';

$SESSION_TOKEN = $_SESSION["user_token"] ?? NULL;

$sql = "SELECT token FROM member WHERE token = '$SESSION_TOKEN'";
$TOKEN_EXIST = $koneksi->query($sql)->num_rows;

if (isset($SESSION_TOKEN) && $TOKEN_EXIST === 1) return header("Location: ../.");
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Recaptcha -->
    <!-- <script src="https://www.google.com/recaptcha/api.js" async defer></script> -->

    <!-- Custom CSS -->
    <link rel="stylesheet" href="../style/style.css?ver=6">

    <title>Controller Login</title>
</head>

<body class="text-light" style="background-color: #0c0c0c">
    <div class="container mt-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-lg-6 bg-gradient-pink rounded p-5">
                <h3 class="mb-4">Controller Login</h3>
                <form id="loginForm" class="needs-validation my-3" novalidate>
                    <div class="mb-3">
                        <label for="inputToken" class="form-label">Token</label>
                        <input type="text" class="form-control login-input" id="inputToken" required>
                        <div class="invalid-feedback rounded bg-dark text-pink p-2">
                            Please input your Token
                        </div>
                        <div class="form-text text-light">Login only with your Private Token.</div>
                    </div>

                    <!-- <div class="g-recaptcha" data-sitekey="6Lf5lfkbAAAAAL1duBtqUotaS6iwXKcPTsougnK7"></div> -->
                    <button id="loginButton" type="submit" class="btn btn-light mt-2" disabled>Login</button>
                    <button id="loadingButton" type="submit" class="btn btn-light mt-2" style="display: none" disabled>
                        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                        <span class="visually-hidden">Loading...</span>
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Bootstrap Bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <!-- Sweetalert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <!-- Custom Script -->
    <script>
        // Form Validation
        (function() {
            'use strict'
            const forms = document.querySelectorAll('.needs-validation')
            Array.prototype.slice.call(forms)
                .forEach(function(form) {
                    form.addEventListener('submit', function(event) {
                        if (!form.checkValidity()) {
                            event.preventDefault()
                            event.stopPropagation()
                        }

                        form.classList.add('was-validated')
                    }, false)
                })
        })()
    </script>

    <script>
        $(document).ready(function() {
            const loginButton = $('#loginButton')
            const loadingButton = $('#loadingButton')
            const invalidFeedback = $('.invalid-feedback')
            loginButton.removeAttr("disabled")

            $('#loginForm').submit(function() {
                event.preventDefault()
                const token = $('#inputToken').val().replace(/\s/g, '');

                if (!token) return $('.invalid-feedback').html('Please input your Token')

                /**
                const response = grecaptcha.getResponse();
                if (response.length === 0) {
                    invalidFeedback.show()
                    invalidFeedback.html('Please complete the captcha.')
                    setInterval(() => {
                        const response = grecaptcha.getResponse();
                        if (response.length !== 0) {
                            clearInterval()
                            invalidFeedback.hide()
                        }
                    }, 1000);
                    return
                }
                **/

                loginButton.hide()
                loadingButton.show()
                $.ajax({
                    type: "POST",
                    url: `login_process.php`,
                    dataType: "json",
                    data: {
                        token,
                        //recaptcha: response
                    },
                    success: function(response) {
                        if (response.code == 200) {
                            let timerInterval
                            Swal.fire({
                                icon: 'success',
                                title: 'Success!',
                                text: response.msg,
                                timer: 1200,
                                timerProgressBar: true,
                                showConfirmButton: false,
                                willClose: () => clearInterval(timerInterval)
                            }).then(() => window.location.href = '../.')
                        }

                        if (response.code !== 200) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Login Failed!',
                                confirmButtonColor: '#f8567b',
                                text: response.msg,
                            })

                            grecaptcha.reset()
                            loginButton.show()
                            loadingButton.hide()
                        }

                    },
                    error: function() {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error!',
                            confirmButtonColor: '#f8567b',
                            text: 'An error occurred, please try again later.',
                        }).then(() => location.reload())
                    }
                });
            })
        })
    </script>
</body>

</html>