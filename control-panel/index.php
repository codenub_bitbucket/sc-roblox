<?php
session_start();
require_once 'koneksi.php';

$SESSION_TOKEN = $_SESSION["user_token"] ?? NULL;

$sql_member = $koneksi->query("SELECT * FROM member WHERE token = '$SESSION_TOKEN'");
$result_member = $sql_member->fetch_assoc();

$TOKEN_EXIST = $sql_member->num_rows;
if (!isset($SESSION_TOKEN) || $TOKEN_EXIST === 0) return header("Location: auth/login");

$webhook_code = $result_member['webhook_code'];
$webhook = $result_member['webhook'];

$expired_at = $result_member['expired_at'];

$date = new DateTime($expired_at);
$now = new DateTime();

if ($date < $now) {
    header("Location: auth/logout");
}

$sql_profile = $koneksi->query("SELECT * FROM profile WHERE webhook_code = '$webhook_code'");
$result_profile = $sql_profile->fetch_assoc();

$fake_id = $result_profile['fake_id'];
$real_username = $result_profile['real_username'];
$fake_username = $result_profile['fake_username'];
$about = $result_profile['about'];
$youtube_url = $result_profile['youtube_url'];
$premium = $result_profile['premium'];

$friends = $result_profile['friends'];
$followers = $result_profile['followers'];
$following = $result_profile['following'];

$domain = $_SERVER['SERVER_NAME'];
$webp_url = "https://$domain/users/$fake_id/profile";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="style/style.css?ver=6">

    <title>Controller</title>
</head>

<body style="background-color: #0c0c0c">
    <div class="container my-5">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-lg-5 bg-gradient-pink text-light rounded p-5">
                <div class="mb-5">
                    <h3 class="float-start">Controller</h3>
                    <a href="auth/logout" class="btn btn-light btn-sm float-end"><i class="bi bi-box-arrow-right"></i> Logout</a>
                </div>
                <div class="content">
                    <h5 class="mb-3">Welcome <span class="fw-bold"><?= $webhook_code ?></span> <i class="bi bi-patch-check" data-bs-toggle="tooltip" data-bs-placement="right" title="Verified ✓"></i></h5>
                    <div class="token mb-5">
                        <h6>Token: </h6>
                        <input id="user-token" type="password" class="bg-dark rounded text-light w-100 p-2" value="<?= $SESSION_TOKEN ?>" disabled>
                        <button id="toggleToken" class="btn btn-dark btn-sm float-end mt-2" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Show/Hide your Token"><i class="bi bi-eye"></i> Show</button>
                    </div>
                    <div class="view-link mb-5">
                        <h6>View & Link: </h6>
                        <div id="user-view" class="bg-dark rounded overflow-auto p-2 mb-2">Profile</div>
                        <div id="user-link" class="bg-dark rounded overflow-auto p-2"><?= $webp_url ?></div>
                        <button class="btn btn-dark btn-sm float-end mt-2" onclick="editView('<?= $real_username ?>', '<?= $fake_username ?>', '<?= $about ?>', '<?= $youtube_url ?>', '<?= $premium ?>', '<?= $friends ?>', '<?= $followers ?>', '<?= $following ?>')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit your View"><i class="bi bi-pencil"></i> Edit</button>
                        <a href="<?= $webp_url ?>" target="_blank" class="btn btn-dark btn-sm float-end mt-2 me-2" onclick="visitView()" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Visit your Website"><i class="bi bi-cursor"></i> Visit</a>
                    </div>
                    <div class="webhook mb-5">
                        <h6>Webhook:</h6>
                        <div class="bg-dark rounded overflow-auto p-2"><?= $webhook ?></div>
                        
                        <button class="btn btn-dark btn-sm float-end mt-2" onclick="editWebhook('<?= $webhook ?>')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit your Webhook"><i class="bi bi-pencil"></i> Edit</button>
                        <button id="testWebhookButton" class="btn btn-dark btn-sm float-end mt-2 me-2" onclick="testWebhook('<?= $webhook ?>')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Tes your Webhook"><i class="bi bi-play"></i> Test</button>
                        
                        <button id="testWebhookLoadingButton" class="btn btn-dark btn-sm float-end mt-2 me-2" style="display: none" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span class="visually-hidden">Loading...</span>
                        </button>
                    </div>
                    <small><i class="bi bi-clock"></i> Expired: <?= $expired_at ?></small>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Bootstrap Bundle -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

    <!-- Sweetalert -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    <!-- Js-Base64 -->
    <script src="https://cdn.jsdelivr.net/npm/js-base64@3.6.1/base64.min.js"></script>
    
    <script>
        const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        const tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
            return new bootstrap.Tooltip(tooltipTriggerEl)
        })
    </script>

    <script>
        function ajax_success_function(response) {
            if (response.code == 200) {
                return Swal.fire({
                    icon: 'success',
                    title: 'Success!',
                    confirmButtonColor: '#f8567b',
                    text: response.msg
                })
            } else {
                return Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    confirmButtonColor: '#f8567b',
                    text: response.msg
                }).then(() => location.reload())
            }
        }

        function ajax_error_function() {
            return Swal.fire({
                icon: 'error',
                title: 'Error!',
                confirmButtonColor: '#f8567b',
                text: 'An error occurred, please try again later.'
            }).then(() => location.reload())
        }
    </script>

    <script>
    $('#toggleToken').click(() => {
        const userToken = $('#user-token')
        
        if (userToken.prop('type') === "password") return userToken.prop('type', 'text');
        userToken.prop('type', 'password');
    })
    </script>

    <script>
        function editViewProcess(real_username, fake_username, about, youtube_url, premium, friends, followers, following) {
            // console.log(real_username, fake_username, about, youtube_url, premium)
            return $.ajax({
                type: "POST",
                url: `function/editView.php`,
                dataType: "json",
                data: {
                    real_username,
                    fake_username,
                    about,
                    youtube_url,
                    premium,
                    friends,
                    followers,
                    following
                },
                success: (response) => ajax_success_function(response).then(() => location.reload()),
                error: () => ajax_error_function()
            });
        }

        function editView(real_username, fake_username, about, youtube_url, premium, friends, followers, following) {
            Swal.fire({
                title: 'Edit View',
                html: `
                <label for="real-username" class="form-label float-start">Real Username</label>
                <input id="real-username" class="form-control mb-3" placeholder="Real Username" value="${real_username}">

                <label for="fake-username" class="form-label float-start">Fake Username</label>
                <input id="fake-username" class="form-control mb-3" placeholder="Fake Username" value="${fake_username}">

                <label for="about" class="form-label float-start">About</label>
                <textarea id="about" class="form-control mb-3" placeholder="About" rows="3">${Base64.decode(about)}</textarea>

                <label for="youtube-url" class="form-label float-start">Youtube URL</label>
                <input id="youtube-url" class="form-control mb-3" placeholder="Youtube URL" value="${youtube_url}">

                <label for="premium" class="form-label float-start">Premium</label>
                <select id="premium" class="form-select mb-3">
                    <option value="1" ${premium == '1' ? 'selected' : ''}>True</option>
                    <option value="0" ${premium == '0' ? 'selected' : ''}>False</option>
                </select>

                <div class="row w-100">
                    <div class="col-sm">
                        <label for="friends" class="form-label float-start">Friends</label>
                        <input id="friends" class="form-control mb-3" placeholder="Friends" value="${friends}">
                    </div>
                    <div class="col-sm">
                      <label for="followers" class="form-label float-start">Followers</label>
                        <input id="followers" class="form-control mb-3" placeholder="Followers" value="${followers}">
                    </div>
                    <div class="col-sm">
                        <label for="following" class="form-label float-start">Following</label>
                        <input id="following" class="form-control mb-3" placeholder="Following" value="${following}">
                    </div>
                </div>
                
                
                `,
                confirmButtonText: 'Edit',
                confirmButtonColor: '#f8567b',
                focusConfirm: false,
                preConfirm: () => {
                    const real_username = Swal.getPopup().querySelector('#real-username').value
                    const fake_username = Swal.getPopup().querySelector('#fake-username').value
                    const about = Swal.getPopup().querySelector('#about').value
                    const youtube_url = Swal.getPopup().querySelector('#youtube-url').value
                    const premium = Swal.getPopup().querySelector('#premium').value
                    const friends = Swal.getPopup().querySelector('#friends').value
                    const followers = Swal.getPopup().querySelector('#followers').value
                    const following = Swal.getPopup().querySelector('#following').value

                    if (!real_username || !fake_username || !about || !youtube_url || !premium || !friends || !followers || !following) return Swal.showValidationMessage(`Please fill all the fields.`)

                    Swal.showLoading()
                    return new Promise((resolve) => $.when(editViewProcess(real_username, fake_username, about, youtube_url, premium, friends, followers, following)).done(() => resolve(true)))
                }
            })
        }
    </script>

    <script>
        function editWebhookProcess(webhook) {
            return $.ajax({
                type: "POST",
                url: `function/editWebhook.php`,
                dataType: "json",
                data: {
                    webhook
                },
                success: (response) => ajax_success_function(response).then(() => location.reload()),
                error: () => ajax_error_function()
            });
        }

        function editWebhook(oldWebhook) {
            Swal.fire({
                title: 'Edit Webhook',
                html: `<textarea id="user-webhook" class="form-control" placeholder="Webhook" rows="5">${oldWebhook}</textarea>`,
                confirmButtonText: 'Edit',
                confirmButtonColor: '#f8567b',
                focusConfirm: false,
                preConfirm: () => {
                    const webhook = Swal.getPopup().querySelector('#user-webhook').value.replace(/\s/g, '')
                    if (!webhook) return Swal.showValidationMessage(`Please enter your webhook`)
                    if (oldWebhook == webhook) return Swal.showValidationMessage(`Nothing has changed`)

                    Swal.showLoading()
                    return new Promise((resolve) => $.when(editWebhookProcess(webhook)).done(() => resolve(true)))
                }
            })
        }
    </script>

    <script>
        function testWebhook(webhook) {
            const testWebhookButton = $('#testWebhookButton')
            const testWebhookLoadingButton = $('#testWebhookLoadingButton')

            testWebhookButton.hide()
            testWebhookLoadingButton.show()

            $.ajax({
                type: "POST",
                url: `function/testWebhook.php`,
                dataType: "json",
                data: {
                    webhook
                },
                success: function(response) {
                    if (response.code === 204) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            confirmButtonColor: '#f8567b',
                            html: `Success sending Test Webhook to <b>${webhook}</b>`,
                        })
                        testWebhookButton.show()
                        testWebhookLoadingButton.hide()
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error!',
                            confirmButtonColor: '#f8567b',
                            text: response.msg,
                        }).then(() => location.reload())
                    }
                },
                error: () => ajax_error_function()
            });
        }
    </script>

</body>

</html>