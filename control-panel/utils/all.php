<?php

function send_response($code, $msg){
    $response["code"] = $code;
    $response["msg"] = $msg;
    
    echo json_encode($response);
}

function remove_all_spaces($string) {
   return preg_replace('/\s+/', '', $string);
}

function remove_all_non_numeric($string){
    return preg_replace('/[^0-9]/', '', $string);
}