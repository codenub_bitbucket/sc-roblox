<?php
session_start();
require_once '../koneksi.php';
require_once '../utils/all.php';

$SESSION_TOKEN = $_SESSION["user_token"] ?? 0;

if (isset($SESSION_TOKEN)) {
    $sql = $koneksi->query("SELECT * FROM member WHERE token = '$SESSION_TOKEN'");
    $TOKEN_EXIST = $sql->num_rows;

    if ($TOKEN_EXIST === 0) {
        return send_response(400, "Session expired.");
    } else {
        $result = $sql->fetch_assoc();
        $expired_at = $result['expired_at'];

        $date = new DateTime($expired_at);
        $now = new DateTime();

        if ($date < $now) return send_response(400, "Session expired.");
    }
} else {
    return send_response(400, "Session expired.");
}

$webhook = remove_all_spaces($_POST["webhook"]) ?? "";

if (!$webhook) return send_response(400, "Please enter your Webhook.");

$sql = "UPDATE member SET webhook='$webhook' WHERE token='$SESSION_TOKEN'";

if ($koneksi->query($sql) === TRUE) {
    echo send_response(200, 'Successfully changed the webhook.');
} else {
    echo send_response(400, $koneksi->error);
}

$koneksi->close();
