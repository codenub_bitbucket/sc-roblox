<?php
session_start();
require_once '../koneksi.php';
require_once '../utils/all.php';

$SESSION_TOKEN = $_SESSION["user_token"] ?? 0;

if (isset($SESSION_TOKEN)) {
    $sql = $koneksi->query("SELECT * FROM member WHERE token = '$SESSION_TOKEN'");
    $TOKEN_EXIST = $sql->num_rows;

    if ($TOKEN_EXIST === 0) {
        return send_response(400, "Session expired.");
    } else {
        $result = $sql->fetch_assoc();
        $expired_at = $result['expired_at'];

        $date = new DateTime($expired_at);
        $now = new DateTime();

        if ($date < $now) return send_response(400, "Session expired.");
    }
} else {
    return send_response(400, "Session expired.");
}

// real_username, fake_username, about, youtube_url, premium

$sql = $koneksi->query("SELECT * FROM member WHERE token = '$SESSION_TOKEN'");
$result = $sql->fetch_assoc();
$webhook_code = $result['webhook_code'];

$real_username = remove_all_spaces($_POST['real_username']);
$fake_username = remove_all_spaces($_POST['fake_username']) ?? "Roblox";
$about = base64_encode($_POST['about']) ?? "";
$youtube_url = remove_all_spaces($_POST['youtube_url']) ?? "https://youtube.com";
$premium = remove_all_non_numeric(remove_all_spaces($_POST['premium'])) ?? 1;

$friends = $_POST['friends'];
$followers = $_POST['followers'];
$following = $_POST['following'];

if (!$real_username) return send_response(400, "Real Username empty.");
if (!$fake_username) return send_response(400, "Fake Username empty.");

if (!$youtube_url) return send_response(400, "Youtube URL empty.");
if (!$premium) return send_response(400, "Premium empty.");

if (!$friends) return send_response(400, "Friends empty.");
if (!$followers) return send_response(400, "Followers empty.");
if (!$following) return send_response(400, "Following empty.");

$sql = "UPDATE profile SET real_username='$real_username', fake_username='$fake_username', about='$about', youtube_url='$youtube_url', premium='$premium', friends='$friends', followers='$followers', following='$following' WHERE webhook_code='$webhook_code'";

if ($koneksi->query($sql) === TRUE) {
    echo send_response(200, 'Successfully updated profile.');
} else {
    echo send_response(400, $koneksi->error);
}

$koneksi->close();
