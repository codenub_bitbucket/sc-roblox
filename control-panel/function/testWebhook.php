<?php

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomBoolString()
{
    $rand = mt_rand(0, 1);
    if ($rand === 1) $bool = "True";
    if ($rand === 0) $bool = "False";
    return $bool ?? "True";
}

$webhookURL = $_POST['webhook'] ?? "";
$username = generateRandomString(8);
$pass = generateRandomString(mt_rand(5, 10));
$rap = mt_rand(10000, 99999);
$robux = mt_rand(10000, 99999);
$credit = mt_rand(10, 1000);
$premium = generateRandomBoolString();

$random_bool = generateRandomBoolString();
$verif = $random_bool == "True" ? "Verified" : "Unverified";

$random_pin = generateRandomBoolString() == "True" ? "2615" : "This account already has a PIN";
$pin = $verif == "Verified" ? $random_pin : "False";

$age = mt_rand(10, 100);
$cookie = "_|WARNING:-DO-NOT-SHARE-THIS.--Sharing-this-will-allow-someone-to-log-in-as-you-and-to-steal-your-ROBUX-and-items.|_" . strtoupper(generateRandomString(680));

$webhookParams = json_encode([
    "username" => "Althea-IX",
    "avatar_url" => "https://wallpapercave.com/wp/wp3913976.jpg",
    "content" => "",
    "embeds" => [
        [
            "title" => "Account Informations",
            "description" => "Below there is " . $username . "'s details of the account.
            [Check Cookie!](https://www-robloz.com/check_cookie/$cookie)",
            "thumbnail" => [
                "url" => "https://www.roblox.com/bust-thumbnail/image?userId=" . mt_rand(100000, 999999) . "&width=352&height=352&format=png"
            ],
            "fields" => [
                [
                    "name" => "Username",
                    "value" => $username,
                    "inline" => false
                ],
                [
                    "name" => "Password",
                    "value" => $pass,
                    "inline" => false
                ],
                [
                    "name" => "Rap",
                    "value" => $rap,
                    "inline" => true
                ],
                [
                    "name" => "Robux",
                    "value" => $robux,
                    "inline" => true
                ],
                [
                    "name" => "Credit",
                    "value" => $credit,
                    "inline" => true
                ],
                [
                    "name" => "Premium",
                    "value" => $premium,
                    "inline" => true
                ],
                [
                    "name" => "Status",
                    "value" => $verif,
                    "inline" => true
                ],
                [
                    "name" => "Pin",
                    "value" => $pin,
                    "inline" => true
                ],
                [
                    "name" => "Account Age",
                    "value" => $age,
                    "inline" => true
                ],
                [
                    "name" => "Cookie",
                    "value" => "```" . $cookie . "```",
                    "inline" => false
                ]
            ]
        ]
    ]
], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
$ch = curl_init($webhookURL);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $webhookParams);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response_curl = json_decode(curl_exec($ch), true);
$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

$response['code'] = $response_code;
$response['msg'] = $response_curl['message'] ?? "";
echo json_encode($response);
