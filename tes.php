<?php

require_once('function.php');



function get_catalog_items($id)
{

    $regex_pattern = '
    /
    \{              # { character
        (?:         # non-capturing group
            [^{}]   # anything that is not a { or }
            |       # OR
            (?R)    # recurses the entire pattern
        )*          # previous group zero or more times
    \}              # } character
    /x
    ';

    $openProxy = file("api/proxies.txt");
    $_proxy = $openProxy[array_rand($openProxy)];
    $_SESSION['proxy'] = $_proxy;
    $splitProxy = explode(':', $_proxy);
    $proxy = $splitProxy[0] . ':' . $splitProxy[1];
    $proxyAuth = $splitProxy[2] . ':' . $splitProxy[3];

    $csrf = get_csrf($proxy . ':' . $proxyAuth);

    $ch = curl_init();
    $header = array();
    $header[] = "Content-Type: application/json";
    $header[] = "Accept: application/json";
    $header[] = "X-CSRF-TOKEN: $csrf";
    $post = '{
    "items": [{
        "itemType": "Asset",
        "id": ' . $id . '
        }]
    }';

    curl_setopt($ch, CURLOPT_URL, "https://catalog.roblox.com/v1/catalog/items/details");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_PROXY, $proxy);
    curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyAuth);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    preg_match_all($regex_pattern, $response, $json);
    return json_decode($json[0][0], true);
}

$catalog = get_catalog_items(48545806);
// $asset_id = $catalog['id'];
// $thumbnail = request("https://thumbnails.roblox.com/v1/assets?assetIds=$asset_id&size=140x140&format=Png&isCircular=false");

print_r($catalog);
// print_r(json_decode($thumbnail, true)['data'][0]['imageUrl']);